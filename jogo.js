var jogador = 0;
var jogadas = 0;

function clique(id) {
	console.log("jogador: "+jogador);
	
	var tag = null;
	
	if(id.currentTarget)
		tag = id.currentTarget; /* currentTarget é para obter o elemento que foi alvo do evento */
	else
		tag = document.getElementById('casa'+id);

	if(tag.style.backgroundImage == '' || tag.style.backgroundImage == null)
	{
		var endereco = 'img/'+jogador+'.jpg';
		tag.style.backgroundImage = 'url('+endereco+')'
        jogadas=jogadas+1
     	var ganhou = 0;
     	if (jogadas>=5){
     	 ganhou = verificarGanhador();
     	 switch(ganhou)
		{
			case 1:
				if(jogador == 0)
					acontecimentos.innerHTML = "Heinz Doofenshmirtz se salvou!";
				else
					acontecimentos.innerHTML = "Perry derrotou Heinz!";
				break;
			case -1:
				acontecimentos.innerHTML = "CHAMA A O.S.U.S.B!!!!!";
				break;
			case 0:
				// ninguem ganhou ainda
				break;
		}
     	}
		acontecimentos = document.getElementById("estragos");



		vez = document.getElementById('vez');
		if(jogador == 0)
		{
			jogador = 1;
			vez.innerHTML = 'Perry';
		}else
		{
			jogador = 0;
			vez.innerHTML = 'Heinz Doofenshmirtz';
		}

		if(ganhou!=0)
		{
			finalizar();
			start = confirm('Deseja iniciar nova partida?');
			if(start)
				iniciar();
		}
	}
}

function verificarGanhador(){
	// pegar elementos
	c1 = document.getElementById('casa1');
	c1 = c1.style.backgroundImage;
	c2 = document.getElementById('casa2');
	c2 = c2.style.backgroundImage;
	c3 = document.getElementById('casa3');
	c3 = c3.style.backgroundImage;
	c4 = document.getElementById('casa4');
	c4 = c4.style.backgroundImage;
	c5 = document.getElementById('casa5');
	c5 = c5.style.backgroundImage;
	c6 = document.getElementById('casa6');
	c6 = c6.style.backgroundImage;
	c7 = document.getElementById('casa7');
	c7 = c7.style.backgroundImage;
	c8 = document.getElementById('casa8');
	c8 = c8.style.backgroundImage;
	c9 = document.getElementById('casa9');
	c9 = c9.style.backgroundImage;
	
	if( (c1==c2 && c2==c3 && c1!='') ||
		(c4==c5 && c5==c6 && c4!='') ||
		(c7==c8 && c8==c9 && c7!='') ||
		(c1==c4 && c4==c7 && c1!='') ||
		(c2==c5 && c5==c8 && c2!='') ||
		(c3==c6 && c6==c9 && c3!='') ||
		(c1==c5 && c5==c9 && c1!='') ||
		(c3==c5 && c5==c7 && c3!=''))
	{
		return 1; // ganhou

	}else if(jogadas == 9){
		return -1;
	}
	return 0;
}

function finalizar(){
	tags = document.getElementsByClassName('casa');

	for(i=0; i<9; i++){
		tags[i].onclick = null;
	}
}

function iniciar() {


	jogadas = 0;

	casas = document.getElementsByClassName('casa');

	for(i=0; i<9; i++){
		casas[i].onclick=clique;
		casas[i].style.backgroundImage='';
	}

	vez = document.getElementById('vez');
	vez.innerHTML = jogador==0?'Heinz Doofenshmirtz' : 'Perry'

}

// 1- verificar ganhador OK
// 2- exibir resultado OK
// 3- Mensagem quando der velha. OK
// 4- Indicar o jogador da vez - OK
// 5- Iniciar nova partida - OK
// 6- Finalização do jogo - OK
// 7- Placar (usando localStorage) https://www.w3schools.com/jsref/prop_win_localstorage.asp

/**
Itens do Trabalho (individual)
1- Melhorar quando se começa a verificar ganhador (a partir da 5 jogada)
2- Melhorar a verificação das linhas para saber se alguém ganhou...
 reduzir de 8 verificações para o mínimo.
4- Trocar alert por mensagem de texto no html.
5- Definir personagens  antagonistas e visual harmonico para o tabuleiro (css)
*/
